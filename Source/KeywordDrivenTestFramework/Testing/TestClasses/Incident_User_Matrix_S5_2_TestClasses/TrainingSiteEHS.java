/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_User_Matrix_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_User_Matrix_V5_2_PageObjects.TrainingSiteEHSPageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "Click on Environment, Health & Safety",
    createNewBrowserInstance = false
)
public class TrainingSiteEHS extends BaseClass
{

    String error = "";

    public TrainingSiteEHS()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!navigateToAPageFromSocialSustainabilityPage())
        {
            return narrator.testFailed("Failed to navigate to a module from Environment, Health & Safety  page" + error);
        }
        return narrator.finalizeTest("Successfully Navigated to module from Environment, Health & Safety  page");
    }

   
    public boolean navigateToAPageFromSocialSustainabilityPage() throws InterruptedException
    {
       
        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteEHSPageObjects.linkForAPageInHomePageXpath(testData.getData("Environment, Health & SafetyPageName")))) {
            error = "Failed to locate the module: "+testData.getData("Environment, Health & SafetyPageName");
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteEHSPageObjects.linkForAPageInHomePageXpath(testData.getData("Environment, Health & SafetyPageName"))))
        {
            error = "Failed to navigate to the module: "+testData.getData("Environment, Health & SafetyPageName");
            return false;
        }

        
        return true;

    }

}
